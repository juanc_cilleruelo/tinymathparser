unit Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.ScrollBox, FMX.Memo, FMX.Controls.Presentation,
  FMX.StdCtrls;

type
  TForm1 = class(TForm)
    BtnExecute: TButton;
    MemoOrigin: TMemo;
    MemoDestiny: TMemo;
    procedure BtnExecuteClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.fmx}

uses TinyMathParser;

procedure TForm1.BtnExecuteClick(Sender: TObject);
var Parser :TTinyMathParser;
begin
   //Parser := TTinyMathParser.Create;
   //Parser.Code.Add('10 + 10');
   //Parser.Run;
   //ShowMessage(FormatFloat('#.##', Parser.ResultValue));

   Parser := TTinyMathParser.Create;
   Parser.Code.Assign(MemoOrigin.Lines);
   Parser.Run;
   MemoDestiny.Lines.Add(FormatFloat('#.##', Parser.ResultValue));
end;

end.
