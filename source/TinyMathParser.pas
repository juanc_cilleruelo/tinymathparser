unit TinyMathParser;
{                                         }
{(LONG = LONG + 10)                       }
{                                         }
{ TokenObjects                            }
{  |__TokenOperador ===>>> Name '('       }
{  |__TokenVariable                       }
{  |      |_ PVariable ===>>> Name LONG   }
{  |__TokenOperador ===>>> Name '='       }
{  |__TokenVariable                       }
{  |      |_ PVariable ===>>> Name LONG   }
{  |__TokenOperador ===>>> Name  '+'      }
{  |__TokenConstant ===>>> Value '10'     }
{  |__TokenOperador ===>>> Name  ')'      }
{                                         }


interface
uses classes;
type
  TObjectClass = (ocNone, ocError, ocConstant, ocVariable, ocOperator, ocFunction, ocDelimLeft, ocDelimRight);
  TVarType     = (vtInteger, vtFloat, vtString, vtBoolean);
  TParserError = (peNone, peBadAssignation, peDivisionByCero, peMissingDelim, peMissignOper, peUndeclaredVar);

  PVariable =^TVariable;
  TVariable = record
    Name  :string;
    Value :Extended;
  end;

  PTokenObject = ^TTokenObject;
  TTokenObject = record
    ObjectClass :TObjectClass;
    Item        :Pointer;
  end;

  PTokenVariable =^TTokenVariable;
  TTokenVariable = record
    Name       :string;
    VarType    :TVarType;
    FloatValue :Extended;
    ValPointer :PVariable;
  end;

  PTokenConstant = ^TTokenConstant;
  TTokenConstant = record
    Value :Extended;
  end;

  PTokenOperator = ^TTokenOperator;
  TTokenOperator = record
    Name        :string;
    Priority    :SmallInt;
    VarFunction :function(X,Y:Extended):Extended of Object;
  end;

  PTokenFunction = ^TTokenFunction;
  TTokenFunction = record
    Name        :string;
    Priority    :SmallInt;
    VarFunction :function(A :Extended):Extended of Object;
  end;

  TTinyMathParser = class
  private
    LineCount      :Integer;
    Tokens         :TStrings;
    TokenObjects   :TList;
    Variables      :TList;
    OrderedObjects :TList;
    ValuesStack    :TList;
    function  ExistVariable(prmName :string):Integer;
    function  AssignVariable(prmName :string):PVariable;
    //== Classifiers
    function  IsDigit   (Value:Char):Boolean;
    function  IsAlpha   (Value:Char):Boolean;
    function  IsOperator(Value:string):Boolean;
    function  IsFunction(Value:string; var Func:string):Boolean;
    function  IsRelOp(Token :string):Boolean;
    //== Creators
    function  NewObject  (prmObjectClass:TObjectClass; prmObjeto:Pointer):PTokenObject;
    function  NewConstant(prmValue:string):PTokenConstant;
    function  NewFunction(prmName :string):PTokenFunction;
    function  NewOperator(prmName :string):PTokenOperator;
    function  NewVariable(prmName:string; prmValue:Extended):PTokenVariable;
    //== Operations
    function  GetTokens(Formula :string):Integer;
    procedure ConvertToObjects;
    procedure RPNObjects; { Reverse Polish Notation }
    procedure ReportError(prmError:TParserError; prmText:string);
    procedure Evaluate;
    //== Auxiliars
    function  GetFromOrderedObjects:PTokenObject;
    function  GetFromValuesStack:Extended;
    function  GetObjectFromValuesStack:PTokenObject;
    function  PutIntoValuesStack(Value:PTokenObject):Boolean;
    //=======================================
    function  FunctionSum      (A, B :Extended):Extended;
    function  FunctionSubstract(A, B :Extended):Extended;
    function  FunctionMultiply (A, B :Extended):Extended;
    function  FunctionDivide   (A, B :Extended):Extended;
    function  FunctionMOD      (A, B :Extended):Extended;
    function  FunctionINT      (A    :Extended):Extended;
    function  FunctionROUND    (A    :Extended):Extended;
    function  FunctionSQRT     (A    :Extended):Extended;
    function  FunctionSQR      (A    :Extended):Extended;
    function  FunctionLN       (A    :Extended):Extended;
    function  FunctionFRAC     (A    :Extended):Extended;
    function  FunctionTRUNC    (A    :Extended):Extended;
    function  FunctionABS      (A    :Extended):Extended;
    function  FunctionEXP      (A    :Extended):Extended;
  public
    Code         :TStringList;
    ResultValue  :Extended;
    Error        :Boolean;
    ErrorMessage :string;
    ErrorLine    :Integer;
    constructor Create;
    destructor  Destroy; override;
    procedure   Run;
    procedure   AddVariable(prmName:string; prmValue:Extended);
    function    GetVariable(prmName:string):Extended;
  end;

implementation

uses System.SysUtils;

const MAX_FUNCTIONS = 8;
      MAX_OPERATORS = 10;

type TFuncsArray     = array[0..MAX_FUNCTIONS] of string;
     TOperatorsArray = array[0..MAX_OPERATORS] of string;

const cnsDIGITS       = ['0'..'9'];
      cnsALPHANUMERIC = ['A'..'Z', 'a'..'z'];
      cnsSPACES       = [#0..#32];
      cnsDELIM_STR    = ['"'];
      cnsOPERATORS :TOperatorsArray = ('=', '+', '-', '*', '/', '%', '^', '~', '(', ')', 'MOD');
      cnsFUNCTIONS :TFuncsArray     = ('INT', 'ROUND', 'SQRT', 'SQR', {'FLOOR',} 'LN', 'FRAC', 'TRUNC', 'ABS', 'EXP');

//=====================================
// E X P R E S I O N   P A R S E R  //
//=====================================
constructor TTinyMathParser.Create;
begin
   inherited Create;
   Code           := TStringList.Create;
   Tokens         := TStringList.Create;
   Variables      := TList.Create;
   TokenObjects   := TList.Create;
   OrderedObjects := TList.Create;
end;

destructor TTinyMathParser.Destroy;
begin
   OrderedObjects.Free;
   TokenObjects.Free;
   Variables.Free;
   Tokens.Free;
   Code.Free;
   inherited Destroy;
end;

procedure TTinyMathParser.Run;
var Formula :string;
begin
   ReportError(peNone, '');
   for formula in Code do begin
      //LineCount := i;
      TokenObjects.Clear;
      GetTokens(Formula.Trim {Trim(Code[LineCount])});
      ConvertToObjects;
      RPNObjects;
      Evaluate;
   end;
end;

function TTinyMathParser.GetTokens(Formula :string):Integer;
var Errors    :string;
    i         :Integer;
    Token     :string;
    Character :Char;
begin
   Tokens.Clear;
   Errors := '';
   Result := 0; {Returns the number of tokens}

   if Length(Formula.Trim) = 0 then Exit;

   i := 1;
   repeat
      if IsDigit(Formula[i]) then begin
         Token := ''; {Init a new token}
         repeat
            Token := Token +  Formula[i];
            Inc(i);
         until not(IsDigit(Formula[i]));  {Until the next letter IsDigit, add to current Token}

         Tokens.Add(Token);  {Add the Token to the list}
         Inc(Result);
      end else
      if IsAlpha(Formula[i]) then begin
         Token := ''; {Init a new token}
         repeat
            Token := Token +  Formula[i]; {Until the next letter IsAlpha, add to the current Token}
            Inc(i);
         until not(IsAlpha(Formula[i]));

         Tokens.Add(Token);  {Add the Token to the list}
         Inc(Result);
      end else
      if IsOperator(Formula[i]) then begin
         Token := Formula[i];
         Tokens.Add(Token);
         Inc(Result);
         Inc(i);
      end
      else Inc(i);

   until i > Length(Formula);
end;

procedure TTinyMathParser.ConvertToObjects;
var i              :Integer;
    tmpObjectClass :TObjectClass;
    ParserFunction :string;
begin
   for i := 0 to Tokens.Count -1 do begin
      if Length(Tokens[i]) > 0 then begin
         if IsDigit(Tokens[i][1]) then begin   { Constants }
            TokenObjects.Add(NewObject(ocConstant, NewConstant(Tokens[i])));
         end else
         if IsOperator(Tokens[i]) then begin { Operators  }
            case Tokens[i][1] of
               ')' :tmpObjectClass := ocDelimRight;
               '(' :tmpObjectClass := ocDelimLeft;
               else tmpObjectClass := ocOperator;
            end;
            TokenObjects.Add(NewObject(tmpObjectClass, NewOperator(Tokens[i])));
         end else
         if IsAlpha(Tokens[i][1]) then begin   { Variables and Functions }
            if IsFunction(Tokens[i], ParserFunction) then begin
               TokenObjects.Add(NewObject(ocFunction, NewFunction(Tokens[i])))
            end
            else begin
               if ExistVariable(Tokens[i]) = -1 then begin
                  ReportError(peUndeclaredVar, Tokens[i]);
               end
               else begin
                  TokenObjects.Add(NewObject(ocVariable, NewVariable(Tokens[i],0)));
               end;
            end;
         end;
      end;
   end;
end;

function TTinyMathParser.NewObject(prmObjectClass :TObjectClass; prmObjeto :Pointer):PTokenObject;
var locObjeto :PTokenObject;
begin
   New(locObjeto);
   locObjeto^.ObjectClass := prmObjectClass;
   locObjeto^.Item        := prmObjeto;
   Result := locObjeto;
end;

function  TTinyMathParser.NewConstant(prmValue :string):PTokenConstant;
var locConstante:PTokenConstant;
begin
   New(locConstante);
   locConstante^.Value := StrToFloat(prmValue);
   Result := locConstante;
end;

function TTinyMathParser.ExistVariable(prmName :string):Integer;
var i :Integer;
begin
   Result := -1;
   for i := 0 to Variables.Count-1 do begin
      if PVariable(Variables[i])^.Name = UpperCase(prmName) then begin
         Result := i;
      end;
   end;
end;

function TTinyMathParser.GetVariable(prmName:string):Extended;
var i     :Integer;
begin
   Result := -1;
   for i := 0 to Variables.Count-1 do begin
      if PVariable(Variables[i])^.Name = UpperCase(prmName) then begin
         Result := PVariable(Variables[i])^.Value;
      end;
   end;
end;

procedure TTinyMathParser.AddVariable(prmName:string; prmValue:Extended);
var locVariable :PVariable;
    Indice      :Integer;
begin
   {if the variable does not exists, create it with the value of the attribute }
   Indice := ExistVariable(prmName);
   if Indice = -1 then begin
      New(locVariable);
      locVariable^.Name  := UpperCase(prmName);
      locVariable^.Value := prmValue;
      Variables.Add(locVariable);
   end { if exists, assign the new value }
   else PVariable(Variables[indice])^.Value := prmValue;
end;

function TTinyMathParser.AssignVariable(prmName :string):PVariable;
var i :Integer;
begin
   i := ExistVariable(prmName);
   if i = -1 then ReportError(peUndeclaredVar, prmName)
             else Result := Variables[i];
end;

function  TTinyMathParser.NewVariable(prmName :string; prmValue :Extended):PTokenVariable;
var locVariable:PTokenVariable;
begin
   New(locVariable);
   locVariable^.Name       := prmName;
   locVariable^.VarType    := vtFloat;
   locVariable^.FloatValue := prmValue;
   locVariable^.ValPointer := AssignVariable(prmName);
   Result := locVariable;
end;

function  TTinyMathParser.NewOperator(prmName :string):PTokenOperator;
var locOperator:PTokenOperator;
begin
   New(locOperator);
   locOperator^.Name     := prmName;
   if prmName = ')' then begin
      locOperator^.Priority    :=  0;
   end else
   if prmName = '=' then begin
      locOperator^.Priority    :=  10;
   end else
   if prmName = '+' then begin
      locOperator^.Priority    :=  20;
      locOperator^.VarFunction := FunctionSum;
   end else
   if prmName = '-' then begin
      locOperator^.Priority    :=  20;
      locOperator^.VarFunction := FunctionSubstract;
   end else
   if prmName = '*' then begin
      locOperator^.Priority    :=  30;
      locOperator^.VarFunction := FunctionMultiply;
   end else
   if prmName = '/' then begin
      locOperator^.Priority    :=  30;
      locOperator^.VarFunction := FunctionDivide;
   end else
   if prmName = 'MOD' then begin
      locOperator^.Priority    :=  30;
      locOperator^.VarFunction := FunctionMOD;
   end else
   if prmName = '%' then begin
      locOperator^.Priority    :=  30;
   end else
   if prmName = '^' then begin
      locOperator^.Priority    :=  40;
   end else
   if prmName = '~' then begin
      locOperator^.Priority    :=  50;
   end else
   if prmName = '(' then begin
      locOperator^.Priority    := 100;
   end;
   Result := locOperator;
end;

function  TTinyMathParser.NewFunction(prmName :string):PTokenFunction;
var locFunction:PTokenFunction;
begin
   New(locFunction);
   locFunction^.Name     := prmName;
   locFunction^.Priority :=  99;
   if prmName = 'INT'   then locFunction^.VarFunction := FunctionINT   else
   if prmName = 'ROUND' then locFunction^.VarFunction := FunctionROUND else
   if prmName = 'SQRT'  then locFunction^.VarFunction := FunctionSQRT  else
   if prmName = 'SQR'   then locFunction^.VarFunction := FunctionSQR   else
   if prmName = 'LN'    then locFunction^.VarFunction := FunctionLN    else
   if prmName = 'FRAC'  then locFunction^.VarFunction := FunctionFRAC  else
   if prmName = 'TRUNC' then locFunction^.VarFunction := FunctionTRUNC else
   if prmName = 'ABS'   then locFunction^.VarFunction := FunctionABS   else
   if prmName = 'EXP'   then locFunction^.VarFunction := FunctionEXP;
   Result := locFunction;
end;

procedure TTinyMathParser.RPNObjects; // In Reverse Polish Notation
var OperatorStack :TList;
    i             :Integer;
    j             :Integer;
    StackClass    :TObjectClass;
    InputClass    :TObjectClass;
    StackPriority :Integer;
    InputPriority :Integer;
begin
   OperatorStack := TList.Create;
   OrderedObjects.Clear;
   try
   for i := 0 to TokenObjects.Count -1 do begin
      InputClass := PTokenObject(TokenObjects[i])^.ObjectClass;
      if (InputClass = ocConstant) or (InputClass = ocVariable) then begin
         OrderedObjects.Add(PTokenObject(TokenObjects[i]));
      end else
      if (InputClass = ocOperator) or (InputClass = ocFunction) then begin
         if OperatorStack.Count > 0 then begin
            StackClass    := PTokenObject(OperatorStack[OperatorStack.Count-1])^.ObjectClass;
            StackPriority := PTokenOperator(PTokenObject(OperatorStack[OperatorStack.Count -1])^.Item)^.Priority;
         end
         else begin
            StackClass    := ocNone;
            StackPriority := 0;
         end;
         InputPriority := PTokenOperator(PTokenObject(TokenObjects[i])^.Item)^.Priority;
         if (OperatorStack.Count > 0) and ((StackClass = ocOperator) or (StackClass = ocFunction)) then begin
            if InputPriority >= StackPriority then begin
               OperatorStack.Add(PTokenObject(TokenObjects[i]));
            end
            else begin
               if (OperatorStack.Count > 0) then begin
                  OrderedObjects.Add(PTokenObject(OperatorStack[OperatorStack.Count-1]));
                  OperatorStack.Delete(OperatorStack.Count -1);
               end;
               OperatorStack.Add(PTokenObject(TokenObjects[i]));
            end;
         end
         else begin
           OperatorStack.Add(PTokenObject(TokenObjects[i]));
         end;
      end else
      if (InputClass = ocDelimLeft) then begin
         OperatorStack.Add(PTokenObject(TokenObjects[i]));
      end else
      if (InputClass = ocDelimRight) then begin
         repeat
            if PTokenObject(OperatorStack[OperatorStack.Count -1])^.ObjectClass <> ocDelimLeft then begin
               OrderedObjects.Add(PTokenObject(OperatorStack[OperatorStack.Count -1]));
               OperatorStack.Delete(OperatorStack.Count -1);
            end;
            if OperatorStack.Count = 0 then begin
               ReportError(peMissingDelim,'');
               Exit;
            end;
         until  PTokenObject(OperatorStack[OperatorStack.Count-1])^.ObjectClass = ocDelimLeft;
         OperatorStack.Delete(OperatorStack.Count -1); // Para Borrar '('
      end;
   end;

   if OperatorStack.Count > 0 then begin
      for j := OperatorStack.Count - 1 downto 0 do begin
         if PTokenObject(OperatorStack[j])^.ObjectClass <> ocDelimLeft then
            OrderedObjects.Add(PTokenObject(OperatorStack[OperatorStack.Count-1]));
         OperatorStack.Delete(OperatorStack.Count -1);
      end;
   end;
   finally OperatorStack.Free;
   end;
end;

function TTinyMathParser.GetFromOrderedObjects:PTokenObject;
begin
   Result := nil;
   if OrderedObjects.Count > 0 then begin
      Result := OrderedObjects[0];
      OrderedObjects.Delete(0);
      OrderedObjects.Pack;
   end;
end;

function TTinyMathParser.GetFromValuesStack:Extended;
var locObject :PTokenObject;
begin
   Result := 0;
   if ValuesStack.Count > 0 then begin
      locObject := ValuesStack[ValuesStack.Count -1];
      ValuesStack.Delete(ValuesStack.Count-1);
      if PTokenObject(locObject)^.ObjectClass = ocVariable
         then Result := PVariable(PTokenVariable(PTokenObject(locObject)^.Item)^.ValPointer)^.Value
         else Result := PTokenConstant(PTokenObject(locObject)^.Item)^.Value;
   end;
end;

function TTinyMathParser.GetObjectFromValuesStack:PTokenObject;
begin
   Result := nil;
   if ValuesStack.Count > 0 then begin
      Result := ValuesStack[ValuesStack.Count -1];
      ValuesStack.Delete(ValuesStack.Count-1);
   end
   else ReportError(peMissignOper,'');
end;

function TTinyMathParser.PutIntoValuesStack(Value:PTokenObject):Boolean;
begin
   if Value <> nil then begin
      ValuesStack.Add(Value);
      Result := True;
   end
   else Result := False;
end;

procedure TTinyMathParser.Evaluate;
var locObject     :PTokenObject;
    CurrentObject :PTokenObject;
    CurrentName   :string;
    Value_1       :Extended;
    Value_2       :Extended;
begin
   CurrentObject := GetFromOrderedObjects;
   if CurrentObject <> nil then begin
      ValuesStack := TList.Create;
      try
         repeat
            case PTokenObject(CurrentObject)^.ObjectClass of
               ocError     :;
               ocConstant  :begin
                 PutIntoValuesStack(CurrentObject);
               end;
               ocVariable  :begin
                 PutIntoValuesStack(CurrentObject);
               end;
               ocOperator  :begin
                  CurrentName := PTokenOperator(PTokenObject(CurrentObject)^.Item)^.Name;
                  if CurrentName = '=' then begin
                     Value_1   := GetFromValuesStack;
                     locObject := GetObjectFromValuesStack;
                     if locObject <> nil then begin
                        if PTokenObject(locObject)^.ObjectClass <> ocVariable then ReportError(peBadAssignation,'')
                        else begin
                           PVariable(PTokenVariable(PTokenObject(locObject)^.Item)^.ValPointer)^.Value := Value_1;
                        end;
                        PutIntoValuesStack(NewObject(ocConstant, NewConstant(FormatFloat('###0.##',Value_1))));
                     end;
                  end
                  else begin
                     Value_1 := GetFromValuesStack;
                     Value_2 := GetFromValuesStack;
                     PutIntoValuesStack(NewObject(ocConstant, NewConstant(FormatFloat('###0.##',PTokenOperator(PTokenObject(CurrentObject)^.Item)^.VarFunction(Value_1,Value_2)))));
                  end;
               end;
               ocFunction  :begin
                  Value_1 := GetFromValuesStack;
                  PutIntoValuesStack(NewObject(ocConstant, NewConstant(FormatFloat('###0.##',PTokenFunction(PTokenObject(CurrentObject)^.Item)^.VarFunction(Value_1)))));
               end;
               else ;{Error(1)}
            end;
            CurrentObject := GetFromOrderedObjects;
         until CurrentObject = nil;
         CurrentObject := ValuesStack[ValuesStack.Count-1];
      finally ValuesStack.Free;
      end;
   end;

   if PTokenObject(CurrentObject)^.ObjectClass = ocVariable
            then ResultValue := GetVariable(PTokenVariable(PTokenObject(CurrentObject)^.Item)^.Name)
            else ResultValue := PTokenConstant(PTokenObject(CurrentObject)^.Item)^.Value;
end;

procedure TTinyMathParser.ReportError(prmError:TParserError; prmText:string);
begin
   case prmError of
      peNone:begin
         Error        := False;
         ErrorLine    := 0;
         ErrorMessage := 'OK';
      end;
      peBadAssignation:begin
         Error        := True;
         ErrorLine    := LineCount;
         ErrorMessage := 'Bad Assignation';
      end;
      peDivisionByCero:begin
         Error        := True;
         ErrorLine    := LineCount;
         ErrorMessage := 'Division by Cero';
      end;
      peMissingDelim:begin
         Error        := True;
         ErrorLine    := LineCount;
         ErrorMessage := 'Missing ''(''';
      end;
      peMissignOper:begin
         Error        := True;
         ErrorLine    := LineCount;
         ErrorMessage := 'Missing operator';
      end;
      peUndeclaredVar:begin
         Error        := True;
         ErrorLine    := LineCount;
         ErrorMessage := 'Undeclared Variable: '''+prmText+'''';
      end;
   end;
end;

//===================================================================================

function TTinyMathParser.IsDigit(Value:Char):Boolean;
begin
   Result := CharInSet(Value, cnsDIGITS) or (Value = '.'{DecimalSeparator});
end;

function TTinyMathParser.IsAlpha(Value:Char):Boolean;
begin
   Result := CharInSet(Value, cnsALPHANUMERIC);
end;

function TTinyMathParser.IsOperator(Value:string):Boolean;
var i :Integer;
begin
   Result := False;
   for i := 0 to MAX_OPERATORS do begin
      if UpperCase(Value) = UpperCase(cnsOPERATORS[i]) then Result := True;
   end;
end;

function TTinyMathParser.IsRelOp(Token :string):Boolean;
begin
   Result := ((Token = '<') or
	             (Token ='<>') or
	             (Token ='<=') or
	             (Token ='>=') or
	             (Token = '=') or
	             (Token = '>'));
end;

function TTinyMathParser.IsFunction(Value:string; var Func:string):Boolean;
var i :Integer;
begin
   Result := False;
   for i := 0 to MAX_FUNCTIONS do begin
      if UpperCase(Value) = UpperCase(cnsFUNCTIONS[i]) then begin
         Func   := cnsFUNCTIONS[i];
         Result := True;
      end;
   end;
end;

//===================================================================================

function TTinyMathParser.FunctionSum(A, B :Extended):Extended;
begin
   result := B + A;
end;

function TTinyMathParser.FunctionSubstract(A, B :Extended):Extended;
begin
   result := B - A;
end;

function TTinyMathParser.FunctionMultiply(A, B :Extended):Extended;
begin
   result := B * A;
end;

function TTinyMathParser.FunctionDivide(A, B :Extended):Extended;
begin
   result := B / A;
end;

function  TTinyMathParser.FunctionMOD(A, B :Extended):Extended;
begin
   result := (Round(B) mod Round(A));
end;

function TTinyMathParser.FunctionINT(A :Extended):Extended;
begin
   result := Int(A);
end;

function TTinyMathParser.FunctionROUND(A :Extended):Extended;
begin
   result := Round(A);
end;

function TTinyMathParser.FunctionSQRT(A :Extended):Extended;
begin
   result := SQRT(A);
end;

function TTinyMathParser.FunctionSQR(A :Extended):Extended;
begin
   result := SQR(A);
end;

function TTinyMathParser.FunctionLN(A :Extended):Extended;
begin
   result := Ln(A);
end;

function TTinyMathParser.FunctionFRAC(A :Extended):Extended;
begin
   result := Frac(A);
end;

function TTinyMathParser.FunctionTRUNC(A :Extended):Extended;
begin
   result := Trunc(A);
end;

function TTinyMathParser.FunctionABS(A :Extended):Extended;
begin
   result := Abs(A);
end;

function TTinyMathParser.FunctionEXP(A :Extended):Extended;
begin
   result := Exp(A);
end;

end.
